package com.raul.sgs.templateproject.data.helpers

enum class ConfirmDialogType {
    warning,
    delete,
    other
}