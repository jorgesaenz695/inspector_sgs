package com.raul.sgs.templateproject.presentation.splash.model

class VersionInUserView(
    val versionNumber: Double = 0.0
)