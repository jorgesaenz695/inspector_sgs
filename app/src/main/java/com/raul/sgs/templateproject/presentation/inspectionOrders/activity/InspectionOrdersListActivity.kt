package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.adapter.InspectionOrdersAdapter
import com.raul.sgs.templateproject.presentation.inspectionOrders.dialog.InformationIoDialog
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.activity_inspection_orders_list.*

class InspectionOrdersListActivity : BaseActivity() {

    lateinit var inspectionOrdersAdapter: InspectionOrdersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection_orders_list)

        setupView()
    }

    fun setupView() {
        setupBrowserRecycler()
        //viewModel.categories.observe(::getLifecycle, ::updateUI)
    }

    private fun setupBrowserRecycler(){
        inspectionOrdersAdapter = InspectionOrdersAdapter()
        inspectionOrdersAdapter.setOnClickListener(onItemClickListener)
        rcvListInspectionOrders.layoutManager = LinearLayoutManager(this)
        rcvListInspectionOrders.adapter = inspectionOrdersAdapter

        dummy()
    }

    private val onItemClickListener = object : InspectionOrdersAdapter.OnItemClickListener{
        override fun onItemClick(psInspectionOrders: InspectionOrders) {
            navigateTo()
        }

        override fun onItemInformationOiClick(psInspectionOrders: InspectionOrders) {
            showDialogInformationInspectionOrder(psInspectionOrders)
        }
    }

    private fun navigateTo(){
        callActivity(this, FirstConstancyActivity())
    }

    private fun showDialogInformationInspectionOrder(psInspectionOrders: InspectionOrders) {
        var fragmentManager = supportFragmentManager
        var informationIoDialog = InformationIoDialog.newInstance("","")
        informationIoDialog.show(fragmentManager,"fragment_info_io")
        informationIoDialog.isCancelable = true
    }

    fun dummy(){
        var dummyList : MutableList<InspectionOrders> = ArrayList()
        var dummyobj = InspectionOrders("","","")
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)

        inspectionOrdersAdapter.goInspectionOrders =  dummyList
    }
}
