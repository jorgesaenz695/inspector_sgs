package com.raul.sgs.templateproject.data.helpers

import org.koin.android.BuildConfig
import android.content.pm.PackageManager
import android.R.attr.versionName
import android.content.Context
import android.content.pm.PackageInfo



class Helper {

    companion object Factory {
        fun getAppVersion(context: Context): Double {
            var versionNameDouble = 0.0
            try {
                val pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0)
                val version = pInfo.versionName
                versionNameDouble = removeCharsToleaveOnlyDoubleChar(version)
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            return versionNameDouble
        }

        fun removeCharsToleaveOnlyDoubleChar(versionName: String): Double {
            val re = Regex("[^0-9. ]")
            val verNameDouble = re.replace(versionName, "").toDouble()
            return verNameDouble
        }

    }

}