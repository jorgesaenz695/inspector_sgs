package com.raul.sgs.templateproject.presentation.inspectionOrders.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.ToolProperty

import java.util.ArrayList


class SpinnerAdapter(val mContext: Context, resourceId: Int, val mValues: MutableList<ToolProperty>) :
    ArrayAdapter<ToolProperty>(mContext, resourceId, mValues) {

    val mChecked: ArrayList<Boolean>

    init {
        mChecked = ArrayList()
        for (i in 0 until mValues.size) {
            mChecked.add(false)
        }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = View.inflate(mContext, R.layout.select_spinner, null)
        val label = row.findViewById(R.id.textoOption) as TextView
        label.setText(mValues[position].toolProperty)
        return row
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row: View
//        if (mValues.size === 1) {
//            row = View.inflate(mContext, R.layout.simple_spinner_nis_one_elem, null)
//        } else {
            row = View.inflate(mContext, R.layout.simple_spinner, null)
//        }

        val label = row.findViewById<View>(R.id.tv_spinnervalue) as TextView
        label.setText(mValues[position].toolProperty)
        for (i in mChecked.indices) {
            mChecked[i] = i == position
        }
        return row
    }
}