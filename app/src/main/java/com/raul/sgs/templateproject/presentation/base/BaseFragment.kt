package com.raul.sgs.templateproject.presentation.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

abstract class BaseFragment : Fragment() {

    private lateinit var baseViewModel: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun callActivity(context: FragmentActivity?, activity: Activity) {
        val intent = Intent(context, activity::class.java)
        startActivity(intent)
    }
}