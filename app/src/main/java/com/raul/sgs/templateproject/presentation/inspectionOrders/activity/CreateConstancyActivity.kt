package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import android.os.Bundle
import android.view.View
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_create_constancy.*
import android.widget.LinearLayout


class CreateConstancyActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_constancy)

        setUpView()
    }

    private fun setUpView(){
        chkExternalWorker.setOnCheckedChangeListener { it, isChecked -> onCkeckExternalWorker(isChecked) }
        btnRegisterConstancy.setOnClickListener { onClickRegisterConstancy() }
        backRegisterConstancy.setOnClickListener{ onBackPressed()}
    }

    private fun onCkeckExternalWorker(isChecked:Boolean){
        if(isChecked) {
            llInforWorkers.visibility = View.VISIBLE
            llExternalWorker.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.4f)
            llInforWorkers.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.6f)
        }else{
            llInforWorkers.visibility = View.GONE
            llExternalWorker.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 2f)
        }
    }

    private fun onClickRegisterConstancy(){
        callActivity(this,RegisterConstancyActivity())
    }
}
