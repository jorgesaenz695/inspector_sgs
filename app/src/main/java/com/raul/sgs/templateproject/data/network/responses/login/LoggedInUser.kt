package com.raul.sgs.templateproject.data.network.responses.login

import com.google.gson.annotations.SerializedName

/**
 * Data class that captures user information for logged in users retrieved from LoginRepositoryImpl
 */

data class LoggedInUser(
    @SerializedName("ID")
    val userId: String,
    @SerializedName("name")
    val firstName: String,
    @SerializedName("last")
    val lastName : String,
    @SerializedName("role")
    val role : String,
    @SerializedName("token")
    val token : String
)
