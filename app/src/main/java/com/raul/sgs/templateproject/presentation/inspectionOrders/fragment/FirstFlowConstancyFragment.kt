package com.raul.sgs.templateproject.presentation.inspectionOrders.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raul.sgs.templateproject.presentation.base.BaseFragment
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.ContextCompat
import android.os.Build
import com.raul.sgs.templateproject.R
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.MotionEvent
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import com.google.android.material.textfield.TextInputLayout
import com.raul.sgs.templateproject.presentation.utils.Keyboard
import com.raul.sgs.templateproject.presentation.utils.SizeAnimation
import com.raul.sgs.templateproject.presentation.utils.Voice
import kotlinx.android.synthetic.main.fragment_first_flow_constancy.*
import kotlinx.android.synthetic.main.view_listering.*
import java.util.*


private val BUNDLE_DATA_USER_LOGIN = "BUNDLE_DATA_USER_LOGIN"

class FirstFlowConstancyFragment : BaseFragment() {

    var user: LoggedInUserView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first_flow_constancy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpView()
    }

    private fun setUpView(){
        eventControls()
        getBundlesBeginOpFrequent()


        checkPermission()
        //startSpeechToText()
    }

    @SuppressLint("RestrictedApi")
    private fun eventControls(){

        llListeringAudio.visibility = View.GONE
        tilDescripServices.visibility = View.GONE
        tilResults.visibility = View.GONE
        tilObservation.visibility = View.GONE
        fabTextFromVoice.visibility = View.GONE

        imvOCDescripServices.setOnClickListener{ showHideTextContent(imvOCDescripServices, tilDescripServices) }
        imvOCResults.setOnClickListener {showHideTextContent(imvOCResults, tilResults)  }
        imvOCObservation.setOnClickListener { showHideTextContent(imvOCObservation, tilObservation) }

        Voice.startSpeechToText(activity as Activity, fabTextFromVoice, edtxtDescripServices, onSpeechListener)

        Keyboard.setKeyboardVisibilityListener(activity as Activity,onKeyboardListener)
    }

    private fun getBundlesBeginOpFrequent(){
        user = arguments!!.getSerializable(BUNDLE_DATA_USER_LOGIN) as LoggedInUserView
    }

    companion object {
        @JvmStatic
        fun newInstance(userView: LoggedInUserView?) =
            FirstFlowConstancyFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_DATA_USER_LOGIN, userView)
                }
            }
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.RECORD_AUDIO) } != PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity!!.packageName))
                startActivity(intent)
                Toast.makeText(activity, "Enable Microphone Permission..!!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun showHideTextContent(
        imvControlOpenClose: ImageView,
        tilContent: TextInputLayout
    ) {
        var goSelectAnimation: Animation?
        goSelectAnimation = SizeAnimation.SizeAnimationLayout(tilContent, if (tilContent.visibility == View.GONE) SizeAnimation.SizeAnimationPhotos.GROW else SizeAnimation.SizeAnimationPhotos.SHRINK)
        (goSelectAnimation).setDuration(200)

        tilContent.clearAnimation()
        tilContent.startAnimation(goSelectAnimation)
        tilContent.visibility = if (tilContent.visibility == View.GONE) View.GONE else View.VISIBLE

        imvControlOpenClose.setImageDrawable(if (tilContent.visibility == View.GONE) resources.getDrawable(R.drawable.ic_close_containt, context!!.theme)  else resources.getDrawable(R.drawable.ic_open_containt, context!!.theme))
    }

    @SuppressLint("RestrictedApi")
    private var onKeyboardListener = object : Keyboard.KeyboardVisibilityListener {
        override fun onKeyboardVisibilityChanged(keyboardVisible: Boolean) {
            if(keyboardVisible){
                fabTextFromVoice.visibility = View.VISIBLE
            }else{
                fabTextFromVoice.visibility = View.GONE
            }
        }

    }

    private var onSpeechListener = object : Voice.SpeechListener {
        override fun onListeringChanged(isListering: Boolean) {
            val animation = AnimationUtils.loadAnimation(context, if(isListering) android.R.anim.slide_in_left else android.R.anim.slide_out_right)
            llListeringAudio.startAnimation(animation)
            llListeringAudio.setVisibility(if(isListering) View.VISIBLE else View.GONE)
        }

    }
}
