package com.raul.sgs.templateproject.presentation.inspectionOrders.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import kotlinx.android.synthetic.main.fragment_third_flow_constancy.*

private val BUNDLE_DATA_USER_LOGIN = "BUNDLE_DATA_USER_LOGIN"

class ThirdFlowConstancyFragment : Fragment() {
    var user: LoggedInUserView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_third_flow_constancy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpView()
    }

    private fun setUpView(){
        eventControls()
        getBundlesBeginOpFrequent()
    }

    private fun eventControls(){
        imvInspector.setOnClickListener { onClickInspector() }
        imvRepresentant.setOnClickListener { onClickRepresentant() }
    }


    private fun onClickInspector(){

    }

    private fun onClickRepresentant(){

    }

    private fun getBundlesBeginOpFrequent(){
        user = arguments!!.getSerializable(BUNDLE_DATA_USER_LOGIN) as LoggedInUserView
    }

    companion object {
        @JvmStatic
        fun newInstance(userView: LoggedInUserView?) =
            ThirdFlowConstancyFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_DATA_USER_LOGIN, userView)
                }
            }
    }
}
