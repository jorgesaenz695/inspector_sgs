package com.raul.sgs.templateproject.data.repository

import com.raul.sgs.templateproject.data.network.responses.login.LoginResponse

interface LoginRepository {

    suspend fun login(username:String, password:String) : LoginResponse
}