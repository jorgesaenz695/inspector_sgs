package com.raul.sgs.templateproject.data.repository.splash

import com.raul.sgs.templateproject.data.network.responses.splash.VersionResponse

interface SplashRepository {
    suspend fun version() : VersionResponse
}