package com.raul.sgs.templateproject.presentation.featureDummyOne

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.raul.sgs.templateproject.R
import kotlinx.android.synthetic.main.dummy_one_fragment.*

class DummyOneFragment : Fragment() {

    companion object {
        fun newInstance() = DummyOneFragment()
    }

    private lateinit var oneViewModel: DummyOneViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dummy_one_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next.setOnClickListener {
            findNavController().navigate(R.id.action_dummyOneFragment_to_dummyTwoFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        oneViewModel = ViewModelProviders.of(this).get(DummyOneViewModel::class.java)
        // TODO: Use the ViewModel
    }



}
