package com.raul.sgs.templateproject.data.helpers

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Html
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.raul.sgs.templateproject.R

class MsgDialog {
    /*
    var dialog: Dialog? = null
    var btn_confirm: Button
    private var iv_typeMsg: ImageView? = null
    private var tv_msg: TextView? = null
    fun MsgDialog(context: Context, type: String, msg: String, redirect: Boolean, listener: View.OnClickListener): ??? {
        dialog = Dialog(context)
        dialog!!.window!!.setLayout(FrameLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dialog_msg)
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        iv_typeMsg = dialog!!.findViewById(R.id.iv_typeMsg) as ImageView
        tv_msg = dialog!!.findViewById(R.id.tv_msg) as TextView
        btn_confirm = dialog!!.findViewById(R.id.btn_confirm) as Button

        if (type == "error") {
            iv_typeMsg!!.setImageResource(R.drawable.ic_error_60dp)
            tv_msg!!.text = msg
        } else if (type == "exito") {
            iv_typeMsg!!.setImageResource(R.drawable.ic_exito_60dp)
            tv_msg!!.text = Html.fromHtml("<b>Éxito!</b><br>$msg")
        } else {
            iv_typeMsg!!.setImageResource(R.drawable.ic_info_60dp)
            tv_msg!!.text = Html.fromHtml("<b>Info</b><br>$msg")
        }

        btn_confirm.setOnClickListener(listener)
    }

    fun MsgDialog(context: Context, type: String, msg: String, redirect: Boolean): ??? {

        dialog = Dialog(context)
        dialog!!.window!!.setLayout(FrameLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dialog_msg)
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        iv_typeMsg = dialog!!.findViewById(R.id.iv_typeMsg) as ImageView
        tv_msg = dialog!!.findViewById(R.id.tv_msg) as TextView
        btn_confirm = dialog!!.findViewById(R.id.btn_confirm) as Button

        if (type == "error") {
            iv_typeMsg!!.setImageResource(R.drawable.ic_error_60dp)
            tv_msg!!.text = msg
        } else if (type == "exito") {
            iv_typeMsg!!.setImageResource(R.drawable.ic_exito_60dp)
            tv_msg!!.text = Html.fromHtml("<b>Éxito!</b><br>$msg")
        } else {
            iv_typeMsg!!.setImageResource(R.drawable.ic_info_60dp)
            tv_msg!!.text = Html.fromHtml("<b>Info</b><br>$msg")
        }

        btn_confirm.setOnClickListener {
            if (redirect) {
                val intent = Intent(context, MainActivity::class.java)
                context.startActivity(intent)
            } else {
                dialog!!.dismiss()
            }
        }

    }

    fun dismiss() {
        if (dialog != null)
            dialog!!.dismiss()
    }

    fun show() {
        if (dialog != null)
            dialog!!.show()
    }*/
}