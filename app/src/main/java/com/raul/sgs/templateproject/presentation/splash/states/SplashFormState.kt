package com.raul.sgs.templateproject.presentation.splash.states

data class SplashFormState (
    val isValidVersion: Boolean = false
)