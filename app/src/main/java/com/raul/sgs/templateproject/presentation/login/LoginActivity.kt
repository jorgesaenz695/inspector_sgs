package com.raul.sgs.templateproject.presentation.login

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.activity.InspectionOrdersListActivity
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

class LoginActivity : BaseActivity(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: LoginViewModelFactory by instance()

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        loginViewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(LoginViewModel::class.java)

        //region llamdas a viewmodel
        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            btn_login?.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                et_username?.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                et_password?.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            progress.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }

            //Complete and destroy login activity once successful
            //finish()
        })

        loginViewModel.example.observe(this@LoginActivity, Observer {
            Toast.makeText(this@LoginActivity, it, Toast.LENGTH_LONG).show()

            callActivity(this,InspectionOrdersListActivity())
        })

        //endregion

        //region ui
        et_username.afterTextChanged {
            loginViewModel.loginDataChanged(
                et_username.text.toString(),
                et_password.text.toString()
            )
        }

        et_password.apply {

            afterTextChanged {
                loginViewModel.loginDataChanged(
                    et_username.text.toString(),
                    et_password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            et_username.text.toString(),
                            et_password.text.toString()
                        )
                }
                false
            }


        }

        btn_login.setOnClickListener {


            /*val service = SGSService.invoke().syncLogin("application/json", "",
                LoginRequest("cramirez", "123")
            ).enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                    Log.e("LOGIN", t.toString())
                }

                override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                    Log.e("LOGIN", "success")
                }
            }
            )*/

            progress.visibility = View.VISIBLE
            loginViewModel.login(et_username.text.toString(), et_password.text.toString())
        }

        //endregion
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName

        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

}

//region helpers

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
//endregion