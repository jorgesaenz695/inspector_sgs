package com.raul.sgs.templateproject.presentation.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.raul.sgs.templateproject.data.repository.splash.SplashRepository

class SplashViewModelFactory(
    private val splashRepository: SplashRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SplashViewModel(splashRepository) as T
    }
}