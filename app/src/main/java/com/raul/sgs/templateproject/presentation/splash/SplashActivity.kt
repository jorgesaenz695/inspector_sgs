package com.raul.sgs.templateproject.presentation.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.data.helpers.ConfirmDialog
import com.raul.sgs.templateproject.data.helpers.ConfirmDialogType
import com.raul.sgs.templateproject.data.helpers.Helper
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.login.LoginActivity
import com.raul.sgs.templateproject.presentation.splash.model.VersionInUserView
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

class SplashActivity : BaseActivity(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: SplashViewModelFactory by instance()
    private var confirmDialog = ConfirmDialog()

    private lateinit var splashViewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        splashViewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(SplashViewModel::class.java)

        splashViewModel.splashFormState.observe(this@SplashActivity, Observer {
            val splashState = it ?: return@Observer
            if (splashState.isValidVersion) {
                callActivity(this, LoginActivity())
            }
        })

        splashViewModel.splashAlertState.observe(this@SplashActivity, Observer {
            val splashState = it ?: return@Observer
            confirmDialog.ConfirmDialog(
                this@SplashActivity,
                splashState.isAlertMessage,
                ConfirmDialogType.warning
            )

            confirmDialog.btn_confirm?.setOnClickListener(View.OnClickListener {
                confirmDialog.dialog?.dismiss()
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                } catch (anfe: android.content.ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                        )
                    )
                }
            })
            confirmDialog.btn_cancel?.setOnClickListener(View.OnClickListener {
                confirmDialog.dialog?.dismiss()
                finish()
                finishAffinity()
            })
        })

        splashViewModel.splashResult.observe(this@SplashActivity, Observer {
            val splashResult = it ?: return@Observer
            if (splashResult.error != null) {
                showLoginFailed(splashResult.error)
            }
            if (splashResult.success != null) {
                updateUiWithVersion(splashResult.success)
            }
        })
        splashViewModel.version()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        splashViewModel.alertMessageDataChanged(
            "error consumo servicio"
        )
    }

    private fun updateUiWithVersion(model: VersionInUserView) {
        val status = model.versionNumber > Helper.getAppVersion(this)
        splashViewModel.versionDataChanged(
            isValidVersion = status
        )
        if (!status) {
            splashViewModel.alertMessageDataChanged(
                splashViewModel.getVersionMessageError(model.versionNumber)
            )
        }
    }
}