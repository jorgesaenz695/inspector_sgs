package com.raul.sgs.templateproject.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.data.helpers.InternetUtil
import com.raul.sgs.templateproject.presentation.base.states.BaseFormState
import com.raul.sgs.templateproject.presentation.login.states.LoginFormState

class BaseViewModel : ViewModel() {

    private val _baseForm = MutableLiveData<BaseFormState>()
    val baseFormState: LiveData<BaseFormState> = _baseForm

    fun internetConnectionDataChanged(status: Boolean) {
        _baseForm.value = BaseFormState(status)
    }
}