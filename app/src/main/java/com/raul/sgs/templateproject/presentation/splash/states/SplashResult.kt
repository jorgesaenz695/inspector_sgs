package com.raul.sgs.templateproject.presentation.splash.states

import com.raul.sgs.templateproject.presentation.splash.model.VersionInUserView

data class SplashResult (
    val success: VersionInUserView? = null,
    val error: Int? = null
)