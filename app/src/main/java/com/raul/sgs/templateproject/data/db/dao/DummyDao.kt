package com.raul.sgs.templateproject.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.raul.sgs.templateproject.data.db.entity.DummyEntity

@Dao
interface DummyDao {

    /**
     *Dar click en las anotaciones para mas detalle
     **/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(dummyEntities: List<DummyEntity>)

    @Query("SELECT * FROM dummyentity")
    fun getDummies(): LiveData<List<DummyEntity>>

    @Query("SELECT * FROM dummyentity WHERE id = :id")
    fun getDummy(id: Int): LiveData<DummyEntity>

    //Tambien se puede reemplazar con un Query 'DELETE FROM ... ' para mayor personalizacion
    @Delete
    fun delete(dummyEntities: List<DummyEntity>)

}