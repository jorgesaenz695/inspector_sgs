package com.raul.sgs.templateproject.presentation.inspectionOrders.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.item_constancy.view.*

class ConstancyAdapter: RecyclerView.Adapter<ConstancyAdapter.ViewHolder>(){

    var goInspectionOrders: List<InspectionOrders> = arrayListOf()

    lateinit var goOnItemClickListener : OnItemClickListener

    open interface OnItemClickListener{
        fun onItemClick(psInspectionOrders: InspectionOrders)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_constancy, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val inspectionOrder = goInspectionOrders[position]

        holder.crdItemConstancy.setOnClickListener { goOnItemClickListener.onItemClick(inspectionOrder) }
    }


    override fun getItemCount(): Int {
        return goInspectionOrders.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var crdItemConstancy: CardView

        init {
            crdItemConstancy = view.crdItemConstancy
        }

    }

    fun setOnClickListener(poOnItemClickListener: OnItemClickListener) {
        goOnItemClickListener = poOnItemClickListener
    }
}