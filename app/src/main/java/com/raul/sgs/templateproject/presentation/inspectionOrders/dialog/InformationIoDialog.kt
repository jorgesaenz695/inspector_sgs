package com.raul.sgs.templateproject.presentation.inspectionOrders.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.raul.sgs.templateproject.R
import kotlinx.android.synthetic.main.dialog_information_lo.view.*


class InformationIoDialog : DialogFragment(), View.OnClickListener {

    lateinit var imvCloseDialog : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.dialog_information_lo, container, false)

        imvCloseDialog = view.imvCloseDialog

        imvCloseDialog.setOnClickListener(this)

        dialog!!.window!!.attributes.windowAnimations = R.style.AnimDialog

        return view

    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window.setLayout(width, height)
        }
    }

    override fun onClick(v: View?) {
        when (v){
            imvCloseDialog -> {
                dismiss()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InformationIoDialog().apply {
                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
                }
            }
    }
}