package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import com.raul.sgs.templateproject.presentation.itemSelects.adapter.ToolSelectAdapter
import com.raul.sgs.templateproject.presentation.utils.SearchToolbar
import com.raul.sgs.templateproject.presentation.utils.SearchToolbar.circleReveal
import com.raul.sgs.templateproject.presentation.utils.SearchToolbar.item_search
import com.raul.sgs.templateproject.presentation.utils.SearchToolbar.searchtollbar
import kotlinx.android.synthetic.main.activity_select_tool.*
import kotlinx.android.synthetic.main.search_toolbar.*
import kotlinx.android.synthetic.main.view_toolbar_title.*

class SelectToolActivity : BaseActivity() {

    lateinit var toolSelectAdapter: ToolSelectAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_tool)

        setupView()
    }

    fun setupView() {
        configToolbar()
        eventControls()
        setupBrowserRecycler()
    }

    private fun configToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    private fun eventControls(){
        SearchToolbar.setSearchtollbar(this,searchtoolbar, onQueryTextListener)
        crdNewItemTool.setOnClickListener { OnclickNewItem() }
        imvSearch.setOnClickListener { OnClickSearch() }
        imvCloseDialog.setOnClickListener { OnClickCloseLayout() }
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_home, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle item selection
//        when (item.itemId) {
//            R.id.action_search -> {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                    circleReveal(this, 1, true, true)
//                else
//                    searchtollbar.setVisibility(View.VISIBLE)
//
//                item_search.expandActionView()
//                return true
//            }
//            else -> return super.onOptionsItemSelected(item)
//        }
//    }

    private fun setupBrowserRecycler(){
        toolSelectAdapter = ToolSelectAdapter()
        toolSelectAdapter.setOnClickListener(onItemClickListener)
        rcvListTools.layoutManager = LinearLayoutManager(this)
        rcvListTools.adapter = toolSelectAdapter
        rcvListTools.setNestedScrollingEnabled(false)

        dummy()
    }

    private val onItemClickListener = object : ToolSelectAdapter.OnItemClickListener{
        override fun onItemClick(psInspectionOrders: InspectionOrders) {
            navigateToInfoTool()
        }
    }

    private val onQueryTextListener = object : SearchToolbar.OnQueryTextListener{
        override fun onQueryTextChange(text: String) {
            val listUpdate = ArrayList<InspectionOrders>()

            for (item in dummyList) {
                if (item.description.toLowerCase().contains(text.toLowerCase()) || item.name.toLowerCase().contains(text.toLowerCase())) {
                    listUpdate.add(item)
                }
            }

            toolSelectAdapter.updateFilterList(listUpdate)
        }

    }

    private fun OnClickSearch(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            circleReveal(this, 1, true, true)
        else searchtollbar.setVisibility(View.VISIBLE)

        item_search.expandActionView()
    }

    private fun OnClickCloseLayout(){
        onBackPressed()
    }

    private fun OnclickNewItem(){
        navigateToInfoTool()
    }

    private fun navigateToInfoTool(){
        callActivity(this, InfoToolActivity())
    }

    var dummyList: MutableList<InspectionOrders> = ArrayList()
    fun dummy(){
        var dummyobj = InspectionOrders("1","Jorge","Saenz")
        dummyList.add(dummyobj)
        dummyobj = InspectionOrders("2","Martin","Paredez")
        dummyList.add(dummyobj)
        dummyobj = InspectionOrders("2","","")
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)

        toolSelectAdapter.goToolList =  dummyList
    }

}
