package com.raul.sgs.templateproject.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.data.repository.LoginRepository
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import com.raul.sgs.templateproject.presentation.login.states.LoginFormState
import com.raul.sgs.templateproject.presentation.login.states.LoginResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val _example = MutableLiveData<String>()
    val example: LiveData<String> = _example

    fun login(username: String, password: String) {

        viewModelScope.launch(Dispatchers.IO) {

            val loginResult = loginRepository.login(username, password)

            if (loginResult.status == 0 ) {

                _loginResult.postValue(LoginResult(
                    success = LoggedInUserView(displayName = "${loginResult.data.firstName} ${loginResult.data.lastName}" )))

            } else {
                _loginResult.postValue(LoginResult(error = R.string.login_failed))
            }

            launch(Dispatchers.Main) {
                _example.value = "Hilo principal"
            }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value =
                LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value =
                LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 1;
    }
}
