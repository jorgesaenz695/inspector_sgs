package com.raul.sgs.templateproject.presentation.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.raul.sgs.templateproject.data.helpers.InternetUtil
import com.raul.sgs.templateproject.presentation.login.LoginViewModel
import com.raul.sgs.templateproject.presentation.login.LoginViewModelFactory
import okhttp3.internal.Internal.instance
import org.kodein.di.generic.instance

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var baseViewModel: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        baseViewModel = ViewModelProviders.of(
            this
        )
            .get(BaseViewModel::class.java)

        baseViewModel.baseFormState.observe(this@BaseActivity, Observer {
            val baseState = it ?: return@Observer
            if (baseState.isInternetConnectionValid) {
                Toast.makeText(applicationContext, "cambio conexion internet", Toast.LENGTH_SHORT).show()
            }
        })

        InternetUtil.observe(this, Observer { status ->
            baseViewModel.internetConnectionDataChanged(true)
        })

    }

    fun callActivity(context: Context, activity: Activity) {
        val intent = Intent(context, activity::class.java)
        startActivity(intent)
    }
}