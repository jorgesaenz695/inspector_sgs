package com.raul.sgs.templateproject.presentation.inspectionOrders.model

class ToolProperty {

    var toolProperty: String = ""

    override fun toString(): String {
        return this.toolProperty
    }
}