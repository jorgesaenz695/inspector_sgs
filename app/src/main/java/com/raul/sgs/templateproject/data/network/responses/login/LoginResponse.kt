package com.raul.sgs.templateproject.data.network.responses.login

data class LoginResponse(
    val status:Int,
    val data: LoggedInUser,
    val msg : String
)