package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.fragment.FirstFlowConstancyFragment
import com.raul.sgs.templateproject.presentation.inspectionOrders.fragment.SecondFlowConstancyFragment
import com.raul.sgs.templateproject.presentation.inspectionOrders.fragment.ThirdFlowConstancyFragment
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import kotlinx.android.synthetic.main.view_toolbar_title.*
import kotlin.collections.ArrayList

class RegisterConstancyActivity : BaseActivity() {

    lateinit var viewPager:ViewPager
    lateinit var tabLayout: TabLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_constancy)
        initViews()
        setStatePageAdapter()
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
                val fm = supportFragmentManager
                val ft = fm.beginTransaction()
                val count = fm.backStackEntryCount
                if (count >= 1) {
                    supportFragmentManager.popBackStack()
                }
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    private fun  initViews(){
        toolbar_text_title.text = resources.getString(R.string.value_default_oi)

        viewPager = findViewById(R.id.pager)
        tabLayout = findViewById(R.id.tabs)

        configToolbar()
    }

    private fun configToolbar() {
        setSupportActionBar(toolbar_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar_title.setNavigationIcon(R.drawable.ic_return_constancy)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()

            R.id.action_show_constancy_list -> {
                callActivity(this,ListConstancyActivity())
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setStatePageAdapter(){
        val myViewPageStateAdapter =
            MyViewPageStateAdapter(supportFragmentManager)
        myViewPageStateAdapter.addFragment(FirstFlowConstancyFragment.newInstance(userView = LoggedInUserView("")),resources.getString(R.string.actRegisterConstancy_lblTab_1))
        myViewPageStateAdapter.addFragment(SecondFlowConstancyFragment.newInstance(userView = LoggedInUserView("")),resources.getString(R.string.actRegisterConstancy_lblTab_2))
        myViewPageStateAdapter.addFragment(ThirdFlowConstancyFragment.newInstance(userView = LoggedInUserView("")),resources.getString(R.string.actRegisterConstancy_lblTab_3))
        viewPager.adapter=myViewPageStateAdapter
        tabLayout.setupWithViewPager(viewPager,true)

    }
    class MyViewPageStateAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm){
        val fragmentList:MutableList<Fragment> = ArrayList()
        val fragmentTitleList:MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList.get(position)
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitleList.get(position)
        }

        fun addFragment(fragment:Fragment,title:String){
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_constancy,menu)
        return super.onCreateOptionsMenu(menu)
    }
}