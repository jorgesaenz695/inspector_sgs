package com.raul.sgs.templateproject.data.network.requests

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("user")
    val username: String,
    @SerializedName("pass")
    val password: String
)