package com.raul.sgs.templateproject.presentation.login.states

import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: LoggedInUserView? = null,
    val error: Int? = null
)
