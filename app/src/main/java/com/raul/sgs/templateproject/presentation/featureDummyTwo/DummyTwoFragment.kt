package com.raul.sgs.templateproject.presentation.featureDummyTwo

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.raul.sgs.templateproject.R
import kotlinx.android.synthetic.main.dummy_two_fragment.*

class DummyTwoFragment : Fragment() {

    companion object {
        fun newInstance() = DummyTwoFragment()
    }

    private lateinit var viewModel: DummyTwoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dummy_two_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_back.setOnClickListener {
            findNavController().navigate(R.id.action_dummyTwoFragment_to_dummyOneFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DummyTwoViewModel::class.java)


    }

}
