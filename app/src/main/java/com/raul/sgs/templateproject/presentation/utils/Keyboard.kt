package com.raul.sgs.templateproject.presentation.utils

import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.app.Activity
import android.view.View


object Keyboard {


    open interface KeyboardVisibilityListener {
        fun onKeyboardVisibilityChanged(keyboardVisible: Boolean)
    }

    fun setKeyboardVisibilityListener(activity: Activity, keyboardVisibilityListener: KeyboardVisibilityListener) {
        val contentView = activity.findViewById<View>(android.R.id.content)
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            private var mPreviousHeight: Int = 0

            override fun onGlobalLayout() {
                val newHeight = contentView.getHeight()
                if (mPreviousHeight != 0) {
                    if (mPreviousHeight > newHeight) {
                        // Height decreased: keyboard was shown
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true)
                    } else if (mPreviousHeight < newHeight) {
                        // Height increased: keyboard was hidden
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false)
                    } else {
                        // No change
                    }
                }
                mPreviousHeight = newHeight
            }
        })
    }

}