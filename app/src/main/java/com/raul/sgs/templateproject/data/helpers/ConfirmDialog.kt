package com.raul.sgs.templateproject.data.helpers

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Html
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.raul.sgs.templateproject.R

class ConfirmDialog {

    var dialog: Dialog? = null
    var btn_confirm: Button? = null
    var btn_cancel: Button? = null
    var btn_neutral: Button? = null
    private var tv_msg: TextView? = null
    private var iv_header: ImageView? = null

    fun ConfirmDialog(context: Context, msg: String, type: String, isVisible: Boolean){
        dialog = Dialog(context)
        dialog!!.setContentView(R.layout.dialog_confirm)
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.window!!.setLayout(FrameLayout.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        iv_header = dialog!!.findViewById(R.id.iv_header)
        tv_msg = dialog!!.findViewById(R.id.tv_msg)

        btn_confirm = dialog!!.findViewById(R.id.btn_confirm)
        btn_cancel = dialog!!.findViewById(R.id.btn_cancel)
        btn_neutral = dialog!!.findViewById(R.id.btn_neutral)
        btn_neutral?.visibility = View.VISIBLE
        iv_header!!.setImageResource(R.drawable.ic_warning_circular)
        tv_msg!!.text = Html.fromHtml("<b>Advertencia!</b><br>$msg")


        btn_cancel?.setOnClickListener { dialog!!.dismiss() }
    }

    fun ConfirmDialog(context: Context, msg: String, listener: View.OnClickListener){
        dialog = Dialog(context)
        dialog!!.setContentView(R.layout.dialog_confirm)
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)


        iv_header = dialog!!.findViewById(R.id.iv_header)
        tv_msg = dialog!!.findViewById(R.id.tv_msg)

        btn_confirm = dialog!!.findViewById(R.id.btn_confirm)
        btn_cancel = dialog!!.findViewById(R.id.btn_cancel)
        btn_neutral = dialog!!.findViewById(R.id.btn_neutral)
        btn_neutral?.visibility = View.INVISIBLE
        btn_neutral?.isEnabled = false

        iv_header!!.setImageResource(R.drawable.ic_warning_circular)
        tv_msg!!.text = Html.fromHtml("<b>Advertencia!</b><br>$msg")

        btn_confirm?.setOnClickListener(listener)
        btn_cancel?.setOnClickListener { dialog!!.dismiss() }
    }

    fun ConfirmDialog(context: Context, msg: String, type: ConfirmDialogType) {

        dialog = Dialog(context)
        dialog!!.window!!.setLayout(FrameLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dialog_confirm)
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        iv_header = dialog!!.findViewById(R.id.iv_header)
        tv_msg = dialog!!.findViewById(R.id.tv_msg)

        btn_confirm = dialog!!.findViewById(R.id.btn_confirm)
        btn_cancel = dialog!!.findViewById(R.id.btn_cancel)
        btn_neutral = dialog!!.findViewById(R.id.btn_neutral)
        btn_neutral?.visibility = View.INVISIBLE
        btn_neutral?.isEnabled = false
        if (type == ConfirmDialogType.delete) {
            iv_header!!.setImageResource(R.drawable.ic_delete_dim_gray)
            tv_msg!!.text = Html.fromHtml("<b>Eliminar!</b><br>$msg")
        } else if (type == ConfirmDialogType.warning) {
            iv_header!!.setImageResource(R.drawable.ic_warning_circular)
            tv_msg!!.text = Html.fromHtml("<b>Advertencia!</b><br>$msg")
        } else {
            iv_header!!.setImageResource(R.drawable.ic_warning_circular)
            tv_msg!!.text = Html.fromHtml("<b>Advertencia!</b><br>$msg")
        }

        btn_cancel?.setOnClickListener { dialog!!.dismiss() }

    }

    fun dismiss() {
        if (dialog != null)
            dialog!!.dismiss()
    }

    fun show() {
        if (dialog != null)
            dialog!!.show()
    }

}
