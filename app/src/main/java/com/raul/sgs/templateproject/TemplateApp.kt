package com.raul.sgs.templateproject

import android.app.Application
import com.raul.sgs.templateproject.data.helpers.InternetUtil
import com.raul.sgs.templateproject.data.network.SGSService
import com.raul.sgs.templateproject.data.repository.LoginRepository
import com.raul.sgs.templateproject.data.repository.LoginRepositoryImpl
import com.raul.sgs.templateproject.data.repository.splash.SplashRepository
import com.raul.sgs.templateproject.data.repository.splash.SplashRepositoryImpl
import com.raul.sgs.templateproject.presentation.login.LoginViewModelFactory
import com.raul.sgs.templateproject.presentation.splash.SplashViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class TemplateApp() :Application(), KodeinAware {

    override val kodein = Kodein.lazy {

        import(androidXModule(this@TemplateApp))
        bind() from singleton { SGSService() }
        bind<LoginRepository>() with singleton { LoginRepositoryImpl(instance()) }
        bind() from provider { LoginViewModelFactory(instance()) }
        bind<SplashRepository>() with singleton { SplashRepositoryImpl(instance()) }
        bind() from provider { SplashViewModelFactory(instance()) }
    }

    override fun onCreate() {
        super.onCreate()
        InternetUtil.init(this)
    }


}