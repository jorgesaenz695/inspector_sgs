package com.raul.sgs.templateproject.presentation.otherActivity

import android.os.Bundle
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders
import com.raul.sgs.templateproject.R

import kotlinx.android.synthetic.main.activity_dummy.*

class DummyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dummy)
        setSupportActionBar(toolbar)

        val viewModel = ViewModelProviders.of(this).get(DummyActivityViewModel::class.java)



        fab.setOnClickListener { view ->
            viewModel.increment()
            Toast.makeText(this, viewModel.count.toString() ,  Toast.LENGTH_LONG).show()
        }
    }

}
