package com.raul.sgs.templateproject.presentation.otherActivity

import android.app.Application
import android.content.Context
import android.net.wifi.WifiManager
import androidx.lifecycle.AndroidViewModel

class DummyActivityViewModel(application : Application) : AndroidViewModel(application) {

    public var count : Int = 0

    fun increment() = count++

    private var wifiManager : WifiManager = application.getSystemService(Context.WIFI_SERVICE) as WifiManager

    fun getConfiguredNetworks() = wifiManager.configuredNetworks


}