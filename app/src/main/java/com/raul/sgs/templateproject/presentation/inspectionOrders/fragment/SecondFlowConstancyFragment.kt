package com.raul.sgs.templateproject.presentation.inspectionOrders.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseFragment
import com.raul.sgs.templateproject.presentation.inspectionOrders.activity.InfoToolActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.activity.SelectToolActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.adapter.ToolUseAdapter
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import com.raul.sgs.templateproject.presentation.login.model.LoggedInUserView
import kotlinx.android.synthetic.main.fragment_second_flow_constancy.*

private val BUNDLE_DATA_USER_LOGIN = "BUNDLE_DATA_USER_LOGIN"

class SecondFlowConstancyFragment : BaseFragment() {
    lateinit var toolUseAdapter: ToolUseAdapter
    var user: LoggedInUserView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_second_flow_constancy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpView()
    }

    private fun setUpView(){
        eventControls()
        getBundlesBeginOpFrequent()
        setupBrowserRecycler()
    }

    private fun eventControls(){
        imvAddTool.setOnClickListener { onClickAddTool() }
    }

    private fun getBundlesBeginOpFrequent(){
        user = arguments!!.getSerializable(BUNDLE_DATA_USER_LOGIN) as LoggedInUserView
    }

    private fun setupBrowserRecycler(){
        toolUseAdapter = ToolUseAdapter()
        toolUseAdapter.setOnClickListener(onItemClickListener)
        rcvToolUse.layoutManager = LinearLayoutManager(activity)
        rcvToolUse.adapter = toolUseAdapter
        rcvToolUse.setNestedScrollingEnabled(false)

        dummy()
    }

    private val onItemClickListener = object : ToolUseAdapter.OnItemClickListener{
        override fun onItemClick(psInspectionOrders: InspectionOrders) {
            navigateToInfoTool()
        }
    }
    
    companion object {
        @JvmStatic
        fun newInstance(userView: LoggedInUserView?) =
            SecondFlowConstancyFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BUNDLE_DATA_USER_LOGIN, userView)
                }
            }
    }

    private fun onClickAddTool(){
        callActivity(activity, SelectToolActivity())
    }

    private fun navigateToInfoTool(){
        callActivity(activity, InfoToolActivity())
    }

    fun dummy(){
        var dummyList : MutableList<InspectionOrders> = ArrayList()
        var dummyobj = InspectionOrders("","","")
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)

        toolUseAdapter.goToolUse =  dummyList
    }
}
