package com.raul.sgs.templateproject.data.repository

import com.raul.sgs.templateproject.data.network.SGSService
import com.raul.sgs.templateproject.data.network.requests.LoginRequest
import com.raul.sgs.templateproject.data.network.responses.login.LoginResponse


class LoginRepositoryImpl(val sgsService: SGSService) : LoginRepository {

    override suspend fun login(username: String, password: String): LoginResponse {
        return sgsService.login(
            "application/json",
            "",
            LoginRequest(username, password)
        )
            .await()

    }

}
