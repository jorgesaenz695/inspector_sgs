package com.raul.sgs.templateproject.data.network.responses.splash

import com.google.gson.annotations.SerializedName

class VersionResponse (
    val status:Int,
    @SerializedName("data")
    val versionNumber: Double,
    val msg : String
)