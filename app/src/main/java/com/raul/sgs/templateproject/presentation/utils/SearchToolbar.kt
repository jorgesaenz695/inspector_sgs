package com.raul.sgs.templateproject.presentation.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.provider.Settings.Global.getString
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.MenuItemCompat
import com.raul.sgs.templateproject.R

@SuppressLint("StaticFieldLeak")
object SearchToolbar {

    internal lateinit var searchtollbar: Toolbar
    internal lateinit var act: Activity
    lateinit var search_menu: Menu
    lateinit var item_search: MenuItem
    lateinit var onQueryTextListener:OnQueryTextListener

    open interface OnQueryTextListener{
        fun onQueryTextChange(text: String)
    }

    open fun setSearchtollbar(activity:Activity, toolbar: Toolbar, onQueryTextListener: OnQueryTextListener) {
        this.searchtollbar = toolbar
        this.onQueryTextListener = onQueryTextListener
        act = activity

        if (searchtollbar != null) {
            searchtollbar.inflateMenu(R.menu.menu_search)
            search_menu = searchtollbar!!.getMenu()

            searchtollbar.setNavigationOnClickListener(View.OnClickListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(act, 1, true, false)
                else
                    searchtollbar.setVisibility(View.GONE)
            })

            item_search = search_menu.findItem(R.id.action_filter_search)

            MenuItemCompat.setOnActionExpandListener(item_search, object : MenuItemCompat.OnActionExpandListener{
                override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                    // Do something when collapsed
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(act, 1, true, false)
                    } else
                        searchtollbar.setVisibility(View.GONE)
                    return true
                }

                override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                    // Do something when expanded
                    return true
                }
            })

            initSearchView(act, onQueryTextListener)


        } else
            Log.d("toolbar", "setSearchtollbar: NULL")
    }

    fun initSearchView(context: Context, onQueryTextListener: OnQueryTextListener) {
        val searchView = search_menu!!.findItem(R.id.action_filter_search).getActionView() as SearchView

        // Enable/Disable Submit button in the keyboard
        searchView.setSubmitButtonEnabled(false)
        searchView.setInputType(InputType.TYPE_CLASS_TEXT)
        searchView.setMaxWidth(Integer.MAX_VALUE)

        // Change search close button image
        val closeButton = searchView.findViewById(R.id.search_close_btn) as ImageView
        closeButton.setImageResource(R.drawable.ic_close_dialog)


        // set hint and the text colors
        val txtSearch = searchView.findViewById(R.id.search_src_text) as EditText
        txtSearch.setHint(context.resources.getString(R.string.value_hint_search_view))
        txtSearch.setHintTextColor(Color.DKGRAY)
        txtSearch.setTextColor(context.resources.getColor(R.color.colorPrimary))
        //txtSearch.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(7))


        // set the cursor
        val searchTextView =
            searchView.findViewById(R.id.search_src_text) as AutoCompleteTextView
        try {
            val mCursorDrawableRes = TextView::class.java.getDeclaredField("mCursorDrawableRes")
            mCursorDrawableRes.isAccessible = true
            mCursorDrawableRes.set(
                searchTextView,
                R.drawable.search_cursor
            ) //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (e: Exception) {
            e.printStackTrace()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                //searchContent(query)
                //searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onQueryTextListener.onQueryTextChange(newText)
                return true
            }
        })

    }

    lateinit var myView: Toolbar

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun circleReveal(activity: Activity, posFromRight: Int, containsOverflow: Boolean, isShow: Boolean) {
        myView =  searchtollbar

        var width = myView.getWidth()

        if (posFromRight > 0)
            width -= posFromRight *
                    activity.applicationContext.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) -
                    activity.applicationContext.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) / 2
        if (containsOverflow)
            width -= activity.applicationContext.resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)

        val cx = width
        val cy = myView.getHeight() / 2

        val anim: Animator
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0f, width.toFloat())
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, width.toFloat(), 0f)

        anim.duration = 220.toLong()

        // make the view invisible when the animation is done
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                if (!isShow) {
                    super.onAnimationEnd(animation)
                    myView.setVisibility(View.INVISIBLE)
                }
            }
        })

        // make the view visible and start the animation
        if (isShow)
            myView.setVisibility(View.VISIBLE)

        // start the animation
        anim.start()
    }

}