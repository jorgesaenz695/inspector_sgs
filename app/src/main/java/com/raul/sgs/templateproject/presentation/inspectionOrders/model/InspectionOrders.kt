package com.raul.sgs.templateproject.presentation.inspectionOrders.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

class InspectionOrders (
    val id: String,
    val name: String,
    val description: String
)