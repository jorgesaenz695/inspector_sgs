package com.raul.sgs.templateproject.presentation.inspectionOrders.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.item_inspection_orders.view.*

class InspectionOrdersAdapter: RecyclerView.Adapter<InspectionOrdersAdapter.ViewHolder>(){

    var goInspectionOrders: List<InspectionOrders> = arrayListOf()

    lateinit var goOnItemClickListener : OnItemClickListener

    open interface OnItemClickListener{
        fun onItemClick(psInspectionOrders: InspectionOrders)
        fun onItemInformationOiClick(psInspectionOrders: InspectionOrders)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_inspection_orders, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val inspectionOrder = goInspectionOrders[position]

        holder.llInformationOi.setOnClickListener { goOnItemClickListener.onItemInformationOiClick(inspectionOrder) }

        holder.crdItemInspectionOrder.setOnClickListener { goOnItemClickListener.onItemClick(inspectionOrder) }
    }


    override fun getItemCount(): Int {
        return goInspectionOrders.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var llInformationOi : LinearLayout
        var crdItemInspectionOrder: CardView

        init {
            llInformationOi = view.llInformationOi
            crdItemInspectionOrder = view.crdItemInspectionOrder
        }

    }

    fun setOnClickListener(poOnItemClickListener: OnItemClickListener) {
        goOnItemClickListener = poOnItemClickListener
    }
}