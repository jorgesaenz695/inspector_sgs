package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import com.raul.sgs.templateproject.presentation.inspectionOrders.adapter.ConstancyAdapter
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.activity_constancy_list.*
import kotlinx.android.synthetic.main.view_toolbar_title.*

class ListConstancyActivity : BaseActivity() {
    lateinit var constancyAdapter: ConstancyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constancy_list)

        setupView()
    }

    fun setupView() {

        crdAddNewConstancy.setOnClickListener { navigateTo() }

        configToolbar()
        setupBrowserRecycler()
    }

    private fun configToolbar() {
        setSupportActionBar(toolbar_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar_title.setNavigationIcon(R.drawable.ic_back_toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBrowserRecycler(){
        constancyAdapter = ConstancyAdapter()
        constancyAdapter.setOnClickListener(onItemClickListener)
        rcvListConstancy.layoutManager = LinearLayoutManager(this)
        rcvListConstancy.adapter = constancyAdapter
        rcvListConstancy.setNestedScrollingEnabled(false)

        dummy()
    }

    private val onItemClickListener = object : ConstancyAdapter.OnItemClickListener{
        override fun onItemClick(psInspectionOrders: InspectionOrders) {
            navigateTo()
        }
    }

    private fun navigateTo(){
        callActivity(this, CreateConstancyActivity())
    }

    fun dummy(){
        var dummyList : MutableList<InspectionOrders> = ArrayList()
        var dummyobj = InspectionOrders("","","")
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)
        dummyList.add(dummyobj)

        constancyAdapter.goInspectionOrders =  dummyList
    }
}
