package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import android.os.Bundle
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.activity_first_constancy.*

class FirstConstancyActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_constancy)

        crdCreateConstancy.setOnClickListener { navigateToFirstConstancy() }
    }

    fun navigateToFirstConstancy(){
        callActivity(this,CreateConstancyActivity())
    }
}
