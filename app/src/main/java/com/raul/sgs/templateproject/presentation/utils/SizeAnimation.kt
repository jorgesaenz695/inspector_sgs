package com.raul.sgs.templateproject.presentation.utils

import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.view.animation.Transformation
import android.widget.LinearLayout
import android.widget.RelativeLayout

class SizeAnimation {

    lateinit var viewLayoutParams: RelativeLayout.LayoutParams

    /**
     * Class that contains the appearing/disappearing animation
     */
    open class SizeAnimationPhotos(
        private val view: View?,
        private val grow: Boolean
    ) : ScaleAnimation(if (grow) 0.0f else 1.0f, if (grow) 1.0f else 0.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF,1.0f, Animation.RELATIVE_TO_SELF, 0.5f) {
        //if (grow) 0.0f else 1.0f, if (grow) 1.0f else 0.0f, 1.0f, 1.0f
        private var viewHeight: Int = 0
        private val viewLayoutParams: RelativeLayout.LayoutParams

        init {
            viewLayoutParams = view?.getLayoutParams() as RelativeLayout.LayoutParams
        }

        /**
         * Apply transformation for a time
         *
         * @param interpolatedTime the time
         * @param t                the transformation
         */
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            super.applyTransformation(interpolatedTime, t)
            if (this.view != null) {
                if (grow) {
                    view.visibility = View.VISIBLE
                } else {
                    if (interpolatedTime >= 1.0) {
                        view.visibility = View.GONE
                    }
                }
                viewHeight = view.height

                var a:Double
                if(grow){
                    a = interpolatedTime.toDouble()
                }else {
                    a = 1.0 - interpolatedTime
                }

                val newMarginBottom = (viewHeight * a).toInt()

                viewLayoutParams.setMargins(
                    viewLayoutParams.leftMargin, viewLayoutParams.topMargin,
                    viewLayoutParams.rightMargin, newMarginBottom - viewHeight
                )
                view.parent.requestLayout()
            }
        }

        companion object {

            /**
             * Constant for growing
             */
            open val GROW = true

            /**
             * Constant for shrinking
             */
            open val SHRINK = false
        }
    }

    open class SizeAnimationLayout(private val view: View?,
                                   private val grow: Boolean): ScaleAnimation(1.0f, 1.0f,if (grow) 0.0f else 1.0f, if (grow) 1.0f else 0.0f) {
        private var viewHeight: Int = 0
        private val viewLayoutParams: LinearLayout.LayoutParams

        init {
            viewLayoutParams = view?.getLayoutParams() as LinearLayout.LayoutParams
        }

        /**
         * Apply transformation for a time
         *
         * @param interpolatedTime the time
         * @param t                the transformation
         */
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            super.applyTransformation(interpolatedTime, t)
            if (this.view != null) {
                if (grow) {
                    view.visibility = View.VISIBLE
                } else {
                    if (interpolatedTime >= 1.0) {
                        view.visibility = View.GONE
                    }
                }
                viewHeight = view.height

                var a:Double
                if(grow){
                    a = interpolatedTime.toDouble()
                }else {
                    a = 1.0 - interpolatedTime
                }

                val newMarginBottom = (viewHeight * a).toInt()

                viewLayoutParams.setMargins(
                    viewLayoutParams.leftMargin, viewLayoutParams.topMargin,
                    viewLayoutParams.rightMargin, newMarginBottom - viewHeight
                )
                view.parent.requestLayout()
            }
        }

        companion object {

            /**
             * Constant for growing
             */
            open val GROW = true

            /**
             * Constant for shrinking
             */
            open val SHRINK = false
        }
    }

}