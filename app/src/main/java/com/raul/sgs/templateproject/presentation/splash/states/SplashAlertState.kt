package com.raul.sgs.templateproject.presentation.splash.states

class SplashAlertState (
    val isAlertMessage: String = ""
)