package com.raul.sgs.templateproject.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.raul.sgs.templateproject.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val navigationView: NavigationView by lazy {
        findViewById<NavigationView>(R.id.nav_view)
    }
    val navController: NavController by lazy {
        findNavController(R.id.nav_host_fragment)
    }

    val appBarConfiguration: AppBarConfiguration by lazy {
        AppBarConfiguration(
            setOf(R.id.dummyOneFragment, R.id.dummyTwoFragment),
            drawer_layout
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        navigationView.setupWithNavController(navController)

        toolbar?.setupWithNavController(navController, appBarConfiguration)

    }

}
