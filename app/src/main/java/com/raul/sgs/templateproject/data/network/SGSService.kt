package com.raul.sgs.templateproject.data.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.raul.sgs.templateproject.BuildConfig
import com.raul.sgs.templateproject.data.network.requests.LoginRequest
import com.raul.sgs.templateproject.data.network.responses.login.LoginResponse
import com.raul.sgs.templateproject.data.network.responses.splash.VersionResponse
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface SGSService {

    @POST("users/login")
     fun login(
        @Header("Content-Type") contentType: String,
        @Header("token") token : String,
        @Body loginRequest: LoginRequest)
            : Deferred<LoginResponse>


    @GET("app/version")
    fun version(
        @Header("Content-Type") contentType: String,
        @Header("token") token : String)
            : Deferred<VersionResponse>

    companion object {
        operator fun invoke(): SGSService {

            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)

                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SGSService::class.java)
        }
    }
}