package com.raul.sgs.templateproject.presentation.featureDummyOne

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class DummyOneViewModelFactory(

) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DummyOneViewModel() as T
    }
}