package com.raul.sgs.templateproject.data.repository.splash

import com.raul.sgs.templateproject.data.network.SGSService
import com.raul.sgs.templateproject.data.network.responses.splash.VersionResponse

class SplashRepositoryImpl(val sgsService: SGSService) : SplashRepository {

    override suspend fun version(): VersionResponse {
        return sgsService.version(
            "application/json",
            ""
        )
            .await()

    }

}
