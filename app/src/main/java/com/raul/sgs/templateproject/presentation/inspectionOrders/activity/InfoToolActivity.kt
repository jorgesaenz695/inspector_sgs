package com.raul.sgs.templateproject.presentation.inspectionOrders.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.adapter.SpinnerAdapter
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.ToolProperty
import kotlinx.android.synthetic.main.activity_info_tool.*

class InfoToolActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_tool)

        setupView()
    }


    fun setupView() {
        eventControls()
    }

    private fun eventControls() {

        imvCloseDialog.setOnClickListener { onClickClose() }

        var lsit = ArrayList<ToolProperty>()
        var item = ToolProperty()
        item.toolProperty = "item 1"
        lsit.add(item)
        item = ToolProperty()
        item.toolProperty = "item 2"
        lsit.add(item)
        item = ToolProperty()
        item.toolProperty = "item 3"
        lsit.add(item)

        spnListProperty.setAdapter(SpinnerAdapter(applicationContext, R.layout.simple_spinner, lsit))
    }

    private fun onClickClose(){
        onBackPressed()
    }

}
