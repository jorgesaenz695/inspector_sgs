package com.raul.sgs.templateproject.presentation.itemSelects.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.item_tool_select.view.*

class ToolSelectAdapter: RecyclerView.Adapter<ToolSelectAdapter.ViewHolder>(){

    var goToolList: MutableList<InspectionOrders> = arrayListOf()

    lateinit var goOnItemClickListener : OnItemClickListener

    open interface OnItemClickListener{
        fun onItemClick(psToolUse: InspectionOrders)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_tool_select, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemSelect = goToolList[position]

        holder.crdItemTool.setOnClickListener { goOnItemClickListener.onItemClick(itemSelect) }
    }


    override fun getItemCount(): Int {
        return goToolList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var crdItemTool: CardView

        init {
            crdItemTool = view.crdItemTool
        }

    }

    fun setOnClickListener(poOnItemClickListener: OnItemClickListener) {
        goOnItemClickListener = poOnItemClickListener
    }

    fun updateFilterList(listUpdate: List<InspectionOrders>){
        goToolList = arrayListOf()
        goToolList.addAll(listUpdate)
        notifyDataSetChanged()

    }
}