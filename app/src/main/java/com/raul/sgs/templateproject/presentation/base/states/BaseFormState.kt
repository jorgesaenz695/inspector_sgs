package com.raul.sgs.templateproject.presentation.base.states

class BaseFormState (
    val isInternetConnectionValid: Boolean = false
)