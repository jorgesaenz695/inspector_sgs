package com.raul.sgs.templateproject.presentation.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.data.repository.splash.SplashRepository
import com.raul.sgs.templateproject.presentation.splash.model.VersionInUserView
import com.raul.sgs.templateproject.presentation.splash.states.SplashAlertState
import com.raul.sgs.templateproject.presentation.splash.states.SplashFormState
import com.raul.sgs.templateproject.presentation.splash.states.SplashResult
import com.raul.sgs.templateproject.presentation.splash.states.SplashSnackBarState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SplashViewModel(private val splashRepository: SplashRepository) : ViewModel() {

    private val _splashForm = MutableLiveData<SplashFormState>()
    val splashFormState: LiveData<SplashFormState> = _splashForm

    private val _splashAlert = MutableLiveData<SplashAlertState>()
    val splashAlertState: LiveData<SplashAlertState> = _splashAlert

    private val _splashSnackBar = MutableLiveData<SplashSnackBarState>()
    val splashSnackBarState: LiveData<SplashSnackBarState> = _splashSnackBar

    private val _splashResult = MutableLiveData<SplashResult>()
    val splashResult: LiveData<SplashResult> = _splashResult

    fun version() {

        viewModelScope.launch(Dispatchers.IO) {

            val splashResult = splashRepository.version()

            if (splashResult.status == 0) {

                _splashResult.postValue(
                    SplashResult(
                        success = VersionInUserView(versionNumber = splashResult.versionNumber)
                    )
                )

            } else {
                _splashResult.postValue(SplashResult(error = R.string.login_failed))
            }
        }
    }

    fun versionDataChanged(isValidVersion: Boolean) {
        _splashForm.value = SplashFormState(isValidVersion = isValidVersion)
    }

    fun alertMessageDataChanged(message: String) {
        _splashAlert.value = SplashAlertState(isAlertMessage = message)
    }

    fun snackBarMessageDataChanged(message: String) {
        _splashSnackBar.value = SplashSnackBarState(isSnackBarMessage = message)
    }

    fun getVersionMessageError(version: Double): String {
        val a = "Hay una versión más reciente disponible:"
        val b = "$version"
        val c = "Deseas descargarla?"
        val d = "$a $b $c"
        return d
    }
}
