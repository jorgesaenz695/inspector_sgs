package com.raul.sgs.templateproject.presentation.splash.states

class SplashSnackBarState (
    val isSnackBarMessage: String = ""
)