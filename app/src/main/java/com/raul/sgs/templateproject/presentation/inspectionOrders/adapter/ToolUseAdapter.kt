package com.raul.sgs.templateproject.presentation.inspectionOrders.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.raul.sgs.templateproject.R
import com.raul.sgs.templateproject.presentation.inspectionOrders.model.InspectionOrders
import kotlinx.android.synthetic.main.item_tool_use.view.*

class ToolUseAdapter: RecyclerView.Adapter<ToolUseAdapter.ViewHolder>(){

    var goToolUse: List<InspectionOrders> = arrayListOf()

    lateinit var goOnItemClickListener : OnItemClickListener

    open interface OnItemClickListener{
        fun onItemClick(psToolUse: InspectionOrders)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_tool_use, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val inspectionOrder = goToolUse[position]

        holder.crdItemTool.setOnClickListener { goOnItemClickListener.onItemClick(inspectionOrder) }
    }


    override fun getItemCount(): Int {
        return goToolUse.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var crdItemTool: CardView

        init {
            crdItemTool = view.crdItemTool
        }

    }

    fun setOnClickListener(poOnItemClickListener: OnItemClickListener) {
        goOnItemClickListener = poOnItemClickListener
    }
}