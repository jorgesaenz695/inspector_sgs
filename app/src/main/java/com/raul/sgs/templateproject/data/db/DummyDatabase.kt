package com.raul.sgs.templateproject.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.raul.sgs.templateproject.data.db.dao.DummyDao

import com.raul.sgs.templateproject.data.db.entity.DummyEntity

@Database(
    entities = [DummyEntity::class],
    version = 1
)
abstract class DummyDatabase : RoomDatabase() {

    abstract fun dummyDao(): DummyDao

    companion object {
        @Volatile
        private var instance: DummyDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DummyDatabase::class.java, "dummy.db"
            )
                .build()
    }


}